/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hmap;

/**
 *
 * @author nyamp
 */
public class ModelView {
    String url;
    Hmap data;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Hmap getData() {
        return data;
    }

    public void setData(Hmap data) {
        this.data = data;
    }
}
