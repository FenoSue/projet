/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import generiquedao.Select;
import hmap.Hmap;
import hmap.ModelView;

/**
 *
 * @author nyamp
 */
@Controlleur(url={"Test"},obj="Test",fonction={"read"})
public class Test {
    int id;
    int num;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
    
    public Test[] read() throws Exception {
        Test[] test = (Test[]) new Select().select(new Test(), null);
        return test;
    }
    
    public ModelView urlList() throws Exception {
        Test[] listtest = this.read();
        ModelView mv = new ModelView();
        Hmap hmap = new Hmap();
        hmap.setNom("listetest");
        hmap.setObj(listtest);
        mv.setUrl("listetest.jsp");
        mv.setData(hmap);
        return mv;
    }
}
