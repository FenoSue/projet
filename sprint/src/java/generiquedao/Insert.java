/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generiquedao;

import connexion.Connexion;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Vector;
import outil.Outil;

/**
 *
 * @author nyamp
 */
public class Insert {
    public void getinsert(Object obj,Connection connection) throws Exception {
        String tab = null;
        Statement preparedstatement = null;
        Method methodget = null;
        Outil outil = new Outil();
        int insert = 0;
        if(connection==null) {
            connection = new Connexion().getConnection();
        }
        try {
            String tablename = obj.getClass().getSimpleName();
            Field[] fields = obj.getClass().getDeclaredFields();
            
            String requette = "insert into " + tablename + " (";
            int a = 0;
            String nameField = null;
            for(a=1; a<fields.length; a++) {
                nameField = fields[a].getName();
                if(a!=fields.length-1 && a!=0) {
                    requette = requette + nameField + ",";
                }
                else if(a==fields.length-1) {
                    requette = requette + nameField + ")";
                }
            }
            requette = requette + " values (";
            int i = 0;
            int j = 0;
            String fieldtypename = null;
            for(i=0; i<fields.length; i++) {
                fieldtypename = fields[i].getType().getName();
                methodget = outil.getMethodeGet(obj, fields[i]);
                System.out.println(fieldtypename);
                if(i==0) {
                    requette = requette;
                }
                else if(i!=fields.length-1 && i!=0) {
                    if(outil.isCharacter(fieldtypename)==true) {
                        requette = requette + "'" + methodget.invoke(obj) + "'" + ",";
                    }
                    else {
                        requette = requette + methodget.invoke(obj) + ",";
                    }
                }
                else if (i==fields.length-1){
                    if(outil.isCharacter(fieldtypename)==true) {
                        requette = requette + "'" + methodget.invoke(obj) + "'" + ")";
                    }
                    else {
                        requette = requette + methodget.invoke(obj) + ")";
                    }
                }
            }
            tab = requette;
            System.out.println(tab);
            
            preparedstatement = connection.createStatement();
            insert = preparedstatement.executeUpdate(tab);
        }
        catch(Exception e) {
            System.out.println(e);
            throw e;
        }
        finally {
            if(preparedstatement!=null) { preparedstatement.close(); }
            if(connection!=null) { connection.close(); }
        }
    }
    
    public void insert(Object objet, Connection connection) throws Exception {
        getinsert(objet, connection);
    }
}
